#!/bin/bash

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e
# complain about unset env variables
set -u

TOP="$(dirname "$0")"

PROJECT=uuid
LICENSE=README
SOURCE_DIR="$PROJECT"

# e.g.
# _uuid_version_t _uuid_version = {
#     0x106202,
#     "1.6.2",
#     ...
# };
# So:
# - find the "_uuid_version =" line
# - skip forward two lines
# - extract and print the version number between quotes
VERSION="$(sed -n -E '/_uuid_version *=/{
n
n
s/^ *"(.*)", *$/\1/p
}' "$TOP/$PROJECT/uuid_vers.h")"

if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    autobuild="$(cygpath -u $AUTOBUILD)"
else
    autobuild="$AUTOBUILD"
fi

stage="$(pwd)"

"$autobuild" source_environment > "$stage/variables_setup.sh" || exit 1
. "$stage/variables_setup.sh"

case "$AUTOBUILD_PLATFORM" in
    linux*)
        pushd "$TOP/$SOURCE_DIR"
            # libtool was ignoring the LDFLAGS option so the only way to force
            # both the compile and link steps to use -mNN etc. was to redefine CC
            # as below.  Sorry for the hack.
            CC="${CC:-gcc} -m$AUTOBUILD_ADDRSIZE $LL_BUILD" \
                ./configure --prefix="$stage"
            make
            make install
        popd
        mv lib release
        mkdir -p lib
        mv release lib
    ;;
    *)
        fail "AUTOBUILD_PLATFORM '$AUTOBUILD_PLATFORM' not supported"
    ;;
esac


mkdir -p "$stage/LICENSES"
cp "$TOP/$SOURCE_DIR/$LICENSE" "$stage/LICENSES/$PROJECT.txt"
echo "$VERSION" > "$stage/VERSION.txt"

